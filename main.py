#!/usr/bin/env python
from ymusic import guarded_main


if __name__ == "__main__":
    guarded_main()
