# Yandex.Radio - Unofficial Player
A selenium-based wrapper over the original web player. 
Allows to tweak simple settings such as adding to favorites, pausing, or skipping songs over HTTP.
Provides dbus notifcations. See the available [frontends](#Frontends) below.



## Frontends
| Frontend   | Description                    | Repository                                                  |
| ---------- | ------------------------------ | ----------------------------------------------------------- |
| Ymu-CLI    | A command line interface       | [ymu-cli](https://gitlab.com/OptimalStrategy/ymu-cli)       |
| Ymu-VSCode | A visual studio code extension | [ymu-vscode](https://gitlab.com/OptimalStrategy/ymu-vscode) |
| Ymu-Tray   | A Gtk tray application         | [ymu-tray](https://gitlab.com/OptimalStrategy/ymu-tray)     |

#### Ymu-CLI
![embedded image](images/cli_client.png)

#### Ymu-VSCode
![!embedded image](images/vscode_status_bar.png)
![!embedded image](images/vscode_commands.png)

#### Ymu-Tray
![!embedded image](images/tray_expanded.png)
![!embedded image](images/tray.png) 
![!embedded image](images/like_notification.png)


---

## Development
1. Clone the repository
    ```bash
    $ git clone git@gitlab.com:OptimalStrategy/ymusic-player.git && cd ymusic-player
    ```

2. The server itself is standalone, however, there is a bunch of packages that enhance user experience.
    ```bash
    # pacman -S dbus  # Dbus is needed for notifications
    # pacman -S gtk3 cairo pkgconf gobject-introspection  # These packages provide the captcha GUI
    ```

3. Install `chromedriver`. If you are a Chromium user, you might have `chromedriver` already installed.
    ```bash
    $ yaourt -S chromedriver
    ```

4. Create a virtual environment and install the requirements. You have three options depending 
on the packages you have installed at step (2).
    * If you installed only dbus, use `requirements.txt`
    * If you installed Gtk3 and stuff, use `requirements-linux.txt`
    * If you are a Windows user, use `requirements-windows.txt`

    ```bash
    $ mkvirtualenv ymusic-player --python python3.7
    (ymusic-player)$ pip install -r requirements.txt
    ```

5. Initialize the config directory
    ```bash
    (ymusic-player)$ python main.py --help
    2019-01-23 22:29:35.088 | DEBUG    | ymusic.config:setup:52 - Creating config directory: /home/<user>/.yplayer
    2019-01-23 22:29:35.089 | DEBUG    | ymusic.config:setup:57 - Initialized default config
    2019-01-23 22:29:35.089 | DEBUG    | ymusic.config:setup:61 - Initialized logging directory
    2019-01-23 22:29:35.106 | DEBUG    | ymusic.config:setup:68 - Copied default icon to '/home/<user>/.yplayer/icon.png'
    2019-01-23 22:29:35.106 | DEBUG    | ymusic.config:setup:71 - Initialized tmp directory
    Usage: main.py [OPTIONS] [WHAT]

    Launch the player server or the debug REPL. Settings provided as argument
    override the config.

    Argument `what` is an index/id or a url of a radio, an album or a playlist
    you want to play. By default `what` is 1, meaning the first radio station.
    There are no default values for 'album' and 'playlist'.

    Options:
    --play [radio|album|playlist]  Play a radio, an album or a playlist.
                                    [default: radio]
    -d, --driver TEXT              Path to your chromedriver.
    -l, --login TEXT               Yandex account login or a phone number. You
                                    will be prompted if not provided.
    --ignore-config
    -c, --config FILE              Read settings from a config  [default:
                                    /home/george/.yplayer/yplayer.json]
    --cookies / --no-cookies       Use cookies saved at the end of the previous
                                    session.  [default: True]
    --discard-cookies              Don't save cookies at the end of the session.
    --disable-notifications        Display notifications through a dbus service.
    --headless / --windowed        Launch the driver in headless or windowed
                                    mode  [default: True]
    --remote-auth                  Use remote authentication. The authentication
                                    server will be launched on the same address
                                    as the player server. [NOT IMPLEMENTED]
    --repl                         Launch REPL after authentication.
    --start-tray                   Launch the tray app if it is available.
    --help                         Show this message and exit.
    ```

6. Edit the config according to your liking:
    ```bash
    $ cat ~/.yplayer/yplayer.json
    {
       "use_cookies": true,
       "save_cookies": true,
       "driver": "chromedriver",
       "notifications": true,
       "always_open_captcha_window": false,
       "auth": {
           "login": null,
           "remote_auth": false
       },
       "default_volume": 100,
       "server": {
           "host": "localhost",
           "port": 7447
       },
       "icon": "/home/<user>/.yplayer/icon.png",
       "history": {
           "dump": true,
           "path": "/home/<user>/.yplayer/history.json"
       }
    }
    ```

7. Launch the server and log on to the site:
    ```bash
    (ymusic-player)$ python main.py
 
    ...
 
    2019-01-23 21:06:31.847 | DEBUG    | __main__:main:167 - Starting ymusic server
    Running on http://localhost:7447 (CTRL + C to quit)
    ```

## TODO
1.  [x] Auth + session storage
2.  [x] Radio support (Play, Pause, Next, Favourite)
3.  [x] HTTP server
4.  [ ] Remote authentication
5.  [ ] Tests
6.  [ ] Shortcuts
7.  [x] Captcha detection
8.  [ ] Authentication captcha detection
9.  [ ] Installable package
10. [x] Album/Playlist/Track support
11. [x] Copy song link to clipboard
12. [x] CLI commands
13. [ ] Make the tray app durable to server-side errors
14. [ ] TUI
15. [x] Song history
16. [ ] A command to start both the server and the tray app
17. [ ] Add a command/endpoint to reload the page
18. [ ] Add a command/endpoint to restart the driver
