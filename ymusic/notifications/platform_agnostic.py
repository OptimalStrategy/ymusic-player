"""
Set the Notification class depending on the platform.
"""

import platform


try:
    if platform.system() == "Windows":
        from ymusic.notifications.windows import (
            WindowsNotification as Notification,
            ensure_initialized,
        )
    else:
        from ymusic.notifications.linux import (
            LinuxNotification as Notification,
            ensure_initialized,
        )
except Exception as e:
    import functools
    import traceback
    from loguru import logger

    from ymusic.notifications.notification import BaseNotification
    from ymusic.config import CONFIG_DIR

    error_path = CONFIG_DIR.joinpath("noti_error.txt").absolute()
    logger.error(
        f"Could not import the notification service for the target platform. "
        f"See the error dump for more info: {error_path}"
    )
    traceback.print_stack(file=error_path.open("w"))

    def ensure_initialized(f):
        """
        Needed for compatibility with the linux notification.
        """

        @functools.wraps(f)
        def wrapper(instance: "PlayerNotificationService", *args, **kwargs):
            return f(instance, *args, **kwargs)

        return wrapper

    class DummyNotifcation(BaseNotification):
        def __init__(self, summary="", message="", icon="", duration=5):
            super().__init__(summary, message, icon)

        def init(self, name="") -> None:
            pass

        def show(self) -> None:
            pass

        def update(self, summary="", message="", icon=None):
            pass

    Notification = DummyNotifcation

__all__ = ["Notification", "ensure_initialized"]
