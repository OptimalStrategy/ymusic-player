import functools
import time
from contextlib import AbstractContextManager

from loguru import logger


class duration(AbstractContextManager):
    """
    Provides a bool-able object that returns :code:`True` until the timeout is reached.

    ..code-block:: python

        with _duration(5) as d:
            while d:  # ends in 5 seconds
                try_to_do_stuff()

    """

    def __init__(self, timeout: float):
        self._start = 0
        self._timeout = timeout

    def __call__(self) -> bool:
        if time.time() - self._start >= self._timeout:
            return False
        return True

    def __bool__(self) -> bool:
        return self.__call__()

    def __enter__(self):
        self._start = time.time()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return True if exc_type is None else False

    def __str__(self):
        return f"<_duration: {self._timeout}>"


def disable_if(param_name: str):
    """
    Wraps the provided function so that the given :class:PlayerNotificationSerivce`
    instance will be returned if :code:`param_name` is :code:`False` or does not exist.

    :param param_name: the name of the flag parameter to check
    :return: wrapper
    """

    def outer(f):
        @functools.wraps(f)
        def inner(
            instance: "PlayerNotificationService", *args, **kwargs
        ) -> "PlayerNotificationService":
            if getattr(instance, param_name, False):
                logger.debug("Notifications are disabled. Skipping '{}'.", f.__name__)
                return instance
            logger.debug("Created a notification for '{}'.", f.__name__)
            return f(instance, *args, **kwargs)

        return inner

    return outer
