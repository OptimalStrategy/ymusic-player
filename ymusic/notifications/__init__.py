"""
Provides cross-platform notifications.
"""

from .service import PlayerNotificationService
