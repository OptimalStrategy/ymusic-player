"""
Provides a notification service that shows notifications and keeps track of the played songs.
"""

import time
import traceback
from pathlib import Path

from dataclasses import dataclass
from threading import Thread
from contextlib import suppress
from typing import Dict, List

from loguru import logger
from selenium.common.exceptions import WebDriverException

from ymusic.notifications.platform_agnostic import Notification, ensure_initialized
from ymusic.notifications.utils import disable_if, duration
from ymusic.config import DEFAULT_ICON, DEFAULT_ICON_WINDOWS, IS_WINDOWS

if IS_WINDOWS:  # Windows icons must be .ico
    _ICON_FILE = DEFAULT_ICON_WINDOWS
else:
    _ICON_FILE = DEFAULT_ICON


@dataclass
class _SongInfo:
    song: str
    cover: str


class PlayerNotificationService(object):
    """
    Displays notifications.
    """

    def __init__(
        self,
        summary: str = "Connected",
        message: str = "The player has connected to the server.",
        app_name: str = "Ymusic Player",
        active: bool = True,
    ):
        """

        :param summary: default summary
        :param message: default message
        :param app_name: name of the application (used to initialize notify)
        :param active: disables all notifications if False
        """
        self.summary = summary
        self.message = message

        self.n = Notification(self.summary, self.message, self.icon())

        self._app_name = app_name
        self._is_initialized = False
        self._inactive = not active

        # I don't want to spawn yet another thread so this collection is going to stick here.
        # The only limitation is that there will be no history without notifications.
        self.song_history: List[str] = []
        self._last_song: _SongInfo = _SongInfo("", "")
        self._thread = None

    @property
    def is_active(self) -> bool:
        """
        Returns :code:`True` if the service is configured to show notifications.

        :return: the activity status
        """
        return not self._inactive

    @is_active.setter
    def is_active(self, status: bool) -> None:
        """
        Changes the current activity status to the given one.

        :param status: activity status
        :return: None
        """
        self._inactive = not status

    def _check_if_track_has_changed(self, player: "Player"):
        """
        Shows a notification  each time the track changes.

        :param player: a :class:`Player`  instance
        :return: None
        """
        song = _SongInfo(player.song, player.get_cover_url())

        # Return if the track has not changed yet or is invalid (i.e. not a track)
        if (
            song.song == self._last_song.song
            or song.song in player.default_songs
            or song.song.startswith(tuple(player._DEFAULT_YMUSIC_PREFIXES))
        ):
            return

        # Check if the song has changed but still has the old cover (album covers are not loaded immediately)
        if song.song != self._last_song.song and song.cover == self._last_song.cover:
            # Attempt to save the cover for two seconds
            with duration(2) as d:
                while d and song.cover == self._last_song.cover:
                    song.cover = player.get_cover_url()

        self._last_song = song
        self.song_history.append(song.song)
        self.on_play(song.song, player.save_album_cover()).show()

    def _listener(self, player: "Player", polling: float = 0.3):
        """
        Executes the worker every :code:`polling` seconds.

        :param player: a :class:`Player` instance
        :param polling: the polling interval
        :return: None
        """
        logger.debug("Started the listener thread [polling = {}].", polling)
        while True:
            if self.is_active:
                with suppress(WebDriverException):
                    self._check_if_track_has_changed(player)
            time.sleep(polling)

    def init(self, player: "Player"):
        """
        Initializes the notification backend if it is not initialized yet.

        :return: None
        """
        if not self._is_initialized and self.is_active:
            self.n.init(self._app_name)
            self._is_initialized = True
            logger.debug("Initialized the notification service.")
            self._thread = Thread(target=self._listener, args=(player,), daemon=True)
            self._thread.start()
        else:
            logger.debug(
                "The service is already initialized or configured to be inactive. Skipping."
            )

    def icon(self, _kind: str = None) -> str:
        """
        Returns the app icon.

        :param _kind: the icon kind (not supported)
        :return: path to the icon
        """
        return str(_ICON_FILE)

    @ensure_initialized
    def show(self) -> "PlayerNotificationService":
        """
        Shows the notification.

        :return: self
        """
        if self.is_active and self._is_initialized:
            logger.debug(
                "Displaying the previously created notification:\n{}\n{}",
                self.n.summary,
                min(len(self.n.summary), 10) * "-",
            )
            self.n.show()
        elif not self._is_initialized:
            service_kind = "notification" if IS_WINDOWS else "dbus"
            logger.debug(
                "The notification {} service is not initialized. Skipping the notification.",
                service_kind,
            )
        else:
            logger.debug("The service is not active. Skipping the notification.")
        return self

    @disable_if("_inactive")
    def on_radio_select(
        self, station_name: str, station_index: int
    ) -> "PlayerNotificationService":
        """
        Creates a notification using the given :code:`station_name` and :code:`station_index`.

        :param station_name: name of the new station
        :param station_index: index of the new station
        :return: self
        """
        self.n.update(
            f"Switched to station '{station_name}' (#{station_index + 1})",
            icon=self.icon(_kind="station"),
        )
        return self

    @disable_if("_inactive")
    def on_play(self, song: str, song_art: str = None) -> "PlayerNotificationService":
        """
        Creates an `on_play` notification.

        :param song: the name of the new track
        :param song_art: the absolute path to the album cover
        :return: self
        """
        self.n.update(
            f"Playing {song}", icon=song_art if song_art else self.icon(_kind="song")
        )
        return self

    @disable_if("_inactive")
    def on_like(
        self, action: "LikeResult", song: str, song_art: str = None
    ) -> "PlayerNotificationService":
        """
        Creates an `on_like` notification.

        :param action: a LikeResult instance {ADDED, REMOVED}
        :param song: the name of the liked track
        :param song_art: the absolute path to the album cover
        :return: self
        """
        prefix = "❤️ Liked" if action == action.ADDED else "💔 Disliked"
        self.n.update(
            f"{prefix} {song}", icon=song_art if song_art else self.icon(_kind="song")
        )
        return self

    @disable_if("_inactive")
    def on_error(
        self,
        summary: str,
        verbose_message: str = "See logs for more details",
        exc: bool = False,
    ) -> "PlayerNotificationService":
        """
        Creates an `on_error` notification.

        :param summary: a summary of the error
        :param verbose_message: a verbose explanation
        :param exc: if True, adds the information of the currently handled
                    exception to :code:`verbose_message`.
        :return: self
        """
        if exc:
            verbose_message = (
                f"{verbose_message}\nException information:\n{traceback.format_exc()}"
            )
        self.n.update(
            f"An error has occurred: {summary}", verbose_message, self.icon("error")
        )
        return self
