"""
A Dbus-based notification implementation.
"""

import functools
from typing import Optional

from dbus.exceptions import DBusException
from loguru import logger
from notify2 import Notification, init as notify_init

from ymusic.notifications.notification import BaseNotification


def ensure_initialized(f):
    """
    Wraps the functions so that failed dbus calls will be recovered by re-initializing the app.

    :param f: a function to attempt to recover
    :return: wrapper
    """

    @functools.wraps(f)
    def wrapper(instance: "PlayerNotificationService", *args, **kwargs):
        try:
            return f(instance, *args, **kwargs)
        except DBusException as e:
            logger.exception(e)
            logger.error(
                "Looks like the dbus service has been removed. Attempting to start a new service..."
            )
            notify_init(instance._app_name)
            logger.info(f"Re-executing the failed function `{f.__name__}`:")
            return f(instance, *args, **kwargs)

    return wrapper


class LinuxNotification(BaseNotification):
    def __init__(self, summary, message: str, icon: Optional[str]):
        super().__init__(summary, message, icon)

        self.n = Notification(self.summary, self.message, self.icon)

    def init(self, name: str) -> None:
        notify_init(name)

    def show(self) -> None:
        self.n.show()

    def update(self, summary: str, message: str = "", icon: str = None):
        super().update(summary, message, icon)
        self.n.update(self.summary, self.message, self.icon)
