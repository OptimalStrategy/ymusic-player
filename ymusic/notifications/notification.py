import abc
from typing import Optional


class BaseNotification(metaclass=abc.ABCMeta):
    """
    The base class for all notifications.
    """

    def __init__(self, summary: str, message: str, icon: Optional[str]):
        """

        :param summary: notification title
        :param message: notification body
        :param icon: notification icon
        """
        self.summary = summary
        self.message = message
        self.icon = icon

    @abc.abstractmethod
    def init(self, name: str) -> None:
        """
        Should initialize the notifier if needed.
        The notifier must be capable of showing notifications after this call.
        """

    @abc.abstractmethod
    def show(self) -> None:
        """
        Should show the stored notification.
        """

    @abc.abstractmethod
    def update(self, summary: str, message: str, icon: Optional[str]):
        """
        Should update the stored notification configuration.
        """
        self.summary = summary
        self.message = message
        self.icon = icon
