"""
Based on https://github.com/jithurjacob/Windows-10-Toast-Notifications by @jithurjacob.
"""
import time
import functools
import threading
from pathlib import Path
from typing import Optional

from loguru import logger

from win32con import (
    CW_USEDEFAULT,
    IDI_APPLICATION,
    IMAGE_ICON,
    LR_DEFAULTSIZE,
    LR_LOADFROMFILE,
    WM_DESTROY,
    WM_USER,
    WS_OVERLAPPED,
    WS_SYSMENU,
)
from win32gui import (
    CreateWindow,
    DestroyWindow,
    LoadIcon,
    LoadImage,
    NIF_ICON,
    NIF_INFO,
    NIF_MESSAGE,
    NIF_TIP,
    NIM_ADD,
    NIM_DELETE,
    NIM_MODIFY,
    GetModuleHandle,
    PostQuitMessage,
    RegisterClass,
    UnregisterClass,
    Shell_NotifyIcon,
    UpdateWindow,
    WNDCLASS,
)


from ymusic.notifications.notification import BaseNotification


def ensure_initialized(f):
    """
    Needed for compatibility with the linux notification.
    """

    @functools.wraps(f)
    def wrapper(instance: "PlayerNotificationService", *args, **kwargs):
        return f(instance, *args, **kwargs)

    return wrapper


class WindowsNotification(BaseNotification):
    class_atom = None

    def __init__(
        self, summary: str, message: str, icon: Optional[str], duration: int = 5
    ):
        super().__init__(summary, message, icon)
        self.duration = duration
        self._thread = None

    def init(self, name: str) -> None:
        pass

    def update(self, summary: str, message: str = "", icon: Optional[str] = None):
        super().update(summary, message, icon)

    def _show(self):
        callbacks = {WM_DESTROY: self.on_destroy}

        self.wc = WNDCLASS()
        self.hinst = self.wc.hInstance = GetModuleHandle(None)
        self.wc.lpszClassName = str("YmuNotificationService")
        self.wc.lpfnWndProc = callbacks

        if self.class_atom is None:
            self.class_atom = RegisterClass(self.wc)

        style = WS_OVERLAPPED | WS_SYSMENU
        self.hwnd = CreateWindow(
            self.class_atom,
            "Taskbar",
            style,
            0,
            0,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            0,
            0,
            self.hinst,
            None,
        )

        try:
            UpdateWindow(self.hwnd)

            icon_flags = LR_LOADFROMFILE | LR_DEFAULTSIZE

            if self.icon is not None:
                icon_path = str(Path(self.icon).absolute())

                try:
                    hicon = LoadImage(
                        self.hinst, icon_path, IMAGE_ICON, 0, 0, icon_flags
                    )
                except Exception as e:
                    logger.exception(
                        f"Could not load the notification icon at `{self.icon}`."
                    )
                    raise e

            else:
                hicon = LoadIcon(0, IDI_APPLICATION)

            flags = NIF_ICON | NIF_MESSAGE | NIF_TIP
            nid = (self.hwnd, 0, flags, WM_USER + 20, hicon, "Tooltip")
            Shell_NotifyIcon(NIM_ADD, nid)
            Shell_NotifyIcon(
                NIM_MODIFY,
                (
                    self.hwnd,
                    0,
                    NIF_INFO,
                    WM_USER + 20,
                    hicon,
                    "Balloon Tooltip",
                    self.message,
                    200,
                    self.summary,
                ),
            )

            time.sleep(self.duration)
        finally:
            DestroyWindow(self.hwnd)
            UnregisterClass(self.wc.lpszClassName, None)

    def show(self):
        self._thread = threading.Thread(target=self._show, daemon=True)
        self._thread.start()
        return self._thread

    def on_destroy(self, hwnd, msg, wparam, lparam):
        nid = (self.hwnd, 0)
        Shell_NotifyIcon(NIM_DELETE, nid)
        PostQuitMessage(0)
