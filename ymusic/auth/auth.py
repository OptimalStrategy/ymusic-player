from typing import Optional
from loguru import logger

from ymusic.driver import DriverWrapper
from ymusic.auth.backend import AuthMeta, AbstractAuth
from ymusic.auth.console_auth import ConsoleAuth
from ymusic.auth.remote_auth import RemoteAuth


# TODO: deprecate persistent passwords
def do_auth(
    wrapper: DriverWrapper,
    login: Optional[str],
    password: Optional[str] = None,
    remote_auth: bool = False,
    **auth_kw,
) -> "AuthMeta":
    """
    Checks if the user is authorized. If not, starts the authorization process.

    :param wrapper: a :class:`DriverWrapper` instance
    :param login: user's login
    :param password: user's password (optional, to be removed)
    :param remote_auth: whether to use the remote authentication or not
    :return: authentication meta (may contain sensitive data)
    """
    auth = _make_auth(wrapper, remote_auth, **auth_kw)
    return auth.authenticate(login, password)


def _make_auth(wrapper: DriverWrapper, remote_auth: bool, **auth_kw) -> "AbstractAuth":
    """
    Creates an authenticator based on the `remote_auth` flag.

    :param wrapper: a :class:`DriverWrapper` instance
    :param remote_auth: whether to use the remote authentication or not
    :return: an :class:`AbstractAuth` instance initialized with the given kw arguments.
    """
    if remote_auth:
        logger.debug("Using remote authentication")
        auth_cls = RemoteAuth
    else:
        logger.debug("Using local authentication")
        auth_cls = ConsoleAuth
    return auth_cls(wrapper, **auth_kw)


__all__ = ["do_auth"]
