import platform
from getpass import getpass
from typing import Optional

from loguru import logger
from selenium.webdriver.remote.webelement import WebElement

from ymusic.auth.backend import (
    AbstractAuth,
    recursive_full_form_auth,
    recursive_short_form_auth,
)


class ConsoleAuth(AbstractAuth):
    """
    Console authentication.
    Prompts the user for password/login if needed.
    """

    def __init__(self, *args, always_open_captcha_window: bool = False, **kwargs):
        super().__init__(*args)
        self.always_open_captcha_window = always_open_captcha_window

    def _authenticate(self):
        """
        Attempts to log on to the site.

        :return: None
        """
        # Prompt the user for the login if it wasn't provided
        if not self.meta.login:
            login = input("Enter your email or phone number: ")
            self.meta.login = login

        logger.debug("Logging in as {}", self.meta.login)
        logger.debug("Auth type: {}", self.meta.auth_type)

        if self.meta.is_full_auth:
            return recursive_full_form_auth(self.wrapper, self.meta.login, getpass)

        return recursive_short_form_auth(
            self.wrapper,
            self.meta.login,
            lambda: getpass("Enter the one-time password: "),
        )

    def handle_captcha(self, element: WebElement):
        """
        Asks the user to solve the captcha. Optionally opens a window with the captcha image.

        :param element: captcha element
        :return: None
        """
        filename = super().handle_captcha(element)

        print(
            f"Looks like the evil company does not like your IP address. "
            f"The captcha has been saved here:\n{filename}"
        )

        if self.always_open_captcha_window:
            value = "y"
        else:
            value = input("Open the image in a Gtk window? y/n: ").strip().lower()

        if value == "y":
            window = captcha_dialog(str(filename))
        else:
            window = None

        digits = input("Enter the captcha: ")
        field = self.wrapper.check_class("input__control")
        field.send_keys(digits)

        button = self.wrapper.check_class("form__submit")
        button.click()

        if window:
            logger.debug("Closing the captcha window...")
            window.destroy()


def captcha_dialog(path: str) -> "Optional[Gtk.Window]":
    """
    Opens a window with the captcha image in a child thread.
    Returns the window instance.

    :param path: the path to the captcha image
    :return: the window instance
    """
    import os
    from threading import Thread

    try:
        import gi

        gi.require_version("Gtk", "3.0")
        from gi.repository import Gtk
    except (ImportError, ValueError):
        logger.error("Gtk is not available. Falling back to the system utilities.")
        command = "xdg-open" if platform.platform() == "Linux" else "start"
        value = (
            input(
                f"Gtk is not available. Try to open with the `{command}` command? y/n: "
            )
            .strip()
            .lower()
        )

        if value == "y":
            t = Thread(target=lambda: os.system(f"{command} {path}"), daemon=True)
            t.start()

        return None

    window = Gtk.Window()
    window.set_title("Captcha")
    window.set_default_size(256, 128)

    image = Gtk.Image()
    image.set_from_file(path)

    window.connect("destroy", Gtk.main_quit)
    window.add(image)
    window.show_all()

    logger.debug("Opening the captcha window...")
    t = Thread(target=Gtk.main, daemon=True)
    t.start()
    return window
