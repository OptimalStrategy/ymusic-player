import asyncio
import enum
from threading import Thread, Lock
from typing import Any, Tuple, Optional, Union, Callable
from contextlib import contextmanager, suppress
from unittest.mock import patch, PropertyMock

import requests
from loguru import logger
from quart import Quart, jsonify, request
from selenium.webdriver.remote.webelement import WebElement

from ymusic import DEFAULT_CONFIG
from ymusic.driver import DriverWrapper
from ymusic.auth.backend import (
    AbstractAuth,
    recursive_full_form_auth,
    recursive_short_form_auth,
    FailedAuthenticationError,
    AuthMeta,
)


@contextmanager
def patched_logger() -> None:
    """
    This context manager patches the QueueListener class so it does not start the handler thread.
    This is required because Quart does not close the logging queue when the app shuts down,
    leading to repeated logs when the main server starts up.
    """
    with patch(
        "logging.handlers.QueueListener.start", new_callable=PropertyMock
    ) as start:

        start.return_value = lambda: ()
        yield


class AuthenticationStatus(object):
    class Status(enum.Enum):
        Unknown = 0
        Captcha = 1
        Waiting = 2
        Ongoing = 3
        Success = 4
        Failure = 5

    def __init__(self):
        self.status = self.Status.Unknown
        self.captcha_path: str = None
        self.captcha_solution: str = None

    @property
    def value(self) -> int:
        return self.status.value

    def set_waiting(self):
        logger.debug("Main Thread set to Waiting")
        self.status = self.Status.Waiting

    def is_waiting_for_request(self) -> bool:
        return self.status == self.Status.Waiting

    def set_ongoing(self):
        logger.debug("Main Thread set to Ongoing")
        self.status = self.Status.Ongoing

    def is_processing_request(self) -> bool:
        return self.status == self.Status.Ongoing

    def set_captcha(self, path: str):
        logger.debug("Main Thread set to Captcha")
        self.status = self.Status.Captcha
        self.captcha_path = path

    def has_pending_captcha(self) -> bool:
        return self.status == self.Status.Captcha

    def set_success(self):
        logger.debug("Main Thread set to Success")
        self.status = self.Status.Success

    def is_success(self) -> bool:
        return self.status == self.Status.Success

    def set_failure(self):
        logger.debug("Main Thread set to Failure")
        self.status = self.Status.Failure

    def is_failure(self) -> bool:
        return self.status == self.Status.Failure


class RemoteAuth(AbstractAuth):
    """
    Follows the Remote Authentication protocol below.

    1) Start the server with the status set to Unknown.
    2) Check if the captcha exists.
        2.1) Yes: set the status to Captcha and wait for the solution.
        2.2) No: Proceed to 3.
    3) Set the status to Waiting. Wait for the credentials to attempt.
        3.1) Once received the credentials, set the status to Ongoing and unlock the main thread.
    4) Did (3) succeed?
        4.1) No: Repeat (3) 2 more times.
        4.2) Yes: Proceed to 5.
    5) Set the status to either Success or Failure depending on the result.
    6) Shutdown the server.
    """

    default_host = DEFAULT_CONFIG["server"]["host"]
    default_port = DEFAULT_CONFIG["server"]["port"]

    def __init__(
        self, wrapper: DriverWrapper, host: str = default_host, port: int = default_port
    ):
        super().__init__(wrapper)

        self._host = host
        self._port = port

        self.status = AuthenticationStatus()

        self._lock = Lock()
        self._lock.acquire()  # does not block, acquires lock for the main thread

        self._n_attempts = 0
        self._max_auth_attempts = 3

        self._server = AuthenticationServer(
            self._host, self._port, self.status, self._lock, self._meta_getter
        )

    def _meta_getter(self) -> "Optional[AuthMeta]":
        return self.meta

    def _pre_auth(self) -> "AuthMeta":
        logger.debug("Starting the auth server...")
        self._server.run_in_thread()

        return super()._pre_auth()

    def authenticate(self, login: str = None, password: str = None) -> AuthMeta:
        try:
            return super().authenticate(login, password)
        finally:
            del self._server  # XXX: not sure if this is needed

    def _authenticate(self):
        logger.debug("Logging in as {}", self.meta.login)
        logger.debug("Auth type: {}", self.meta.auth_type)

        logger.debug("Starting the auth loop...")

        while not self.has_reached_limit and not self.meta.success:
            # Allow the server to accept new authentication requests
            self.status.set_waiting()

            # Lock the thread until the auth server receives a request from the client
            logger.debug(
                f"Waiting for an authentication request... "
                f"[attempt = {self._n_attempts} / {self._max_auth_attempts}]"
            )
            self._lock.acquire(blocking=True)

            # Attempt to authenticate
            if self._attempt_auth():
                self.status.set_success()
                break

        try:
            if self.has_reached_limit or not self.meta.success:
                self.status.set_failure()
                raise FailedAuthenticationError(self._n_attempts)
        finally:
            logger.debug("Shutting down the authentication server...")

            # Quart does not provide APIs to shut down an application,
            # so we need to make a shutdown request to the server manually.
            with suppress(requests.exceptions.Timeout):
                requests.get(f"http://{self._host}:{self._port}/shutdown", timeout=1e-5)

    def handle_captcha(self, element: WebElement) -> Any:
        filename = super().handle_captcha(element)

        self.status.set_captcha(filename)

        # Here we wait for the server to receive the captcha solution
        self._lock.acquire(blocking=True)

        digits = self.status.captcha_solution
        field = self.wrapper.check_class("input__control")
        field.send_keys(digits)

        button = self.wrapper.check_class("form__submit")
        button.click()

    def _any_form_auth(self, auth_fn) -> bool:
        try:
            auth_fn(
                self.wrapper, self.meta.login, lambda: self.meta.password, max_depth=1
            )
            return True
        except FailedAuthenticationError:
            return False

    @property
    def has_reached_limit(self) -> bool:
        return self._n_attempts >= self._max_auth_attempts

    def _attempt_auth(self) -> bool:
        auth_fn = (
            recursive_full_form_auth
            if self.meta.is_full_auth
            else recursive_short_form_auth
        )

        logger.debug("Executing the authentication pipeline...")
        result = self._any_form_auth(auth_fn)
        self.meta.status = result
        self._n_attempts += 1

        if result or self.has_reached_limit:
            logger.debug(
                "Authentication completed [result = {}, limit_reached = {}, attempts = {}]",
                result,
                self.has_reached_limit,
                self._n_attempts,
            )

        return result


class AuthenticationServer(object):
    Response = Tuple[str, int]

    def __init__(
        self,
        host: str,
        port: int,
        status: AuthenticationStatus,
        lock: Lock,
        meta: Callable[[], Optional[AuthMeta]],
    ):
        """

        :param player: Player instance
        :param host: server host
        :param port: server port
        """
        self.host = host
        self.port = port
        self.status = status
        self.lock = lock
        self._meta = meta

        self.app = Quart(__name__)
        self._init()

    @property
    def meta(self) -> Optional["AuthMeta"]:
        return self._meta()

    def run_in_thread(self) -> Thread:
        # Both the authentication server and the player server run on the same address.
        # Therefore, if we do not take measures to cleanup the bound sockets, we are going to have
        # the "address is already in use error".
        #
        # Conveniently enough, Quart handles this stuff for us during the event loop shutdown.
        # However,if we were to provide the main thread's event loop, Quart would have stopped it,
        # requiring us to create a new loop before starting the player server.
        #
        # We can avoid this by creating a new even loop right here.
        t = Thread(target=self.run, args=(asyncio.new_event_loop(),), daemon=True)
        t.start()
        return t

    def run(self, loop: asyncio.AbstractEventLoop, *args, **kwargs) -> None:
        """
        Starts the server.
        """
        asyncio.set_event_loop(loop)
        with patched_logger():
            self.app.run(self.host, self.port, *args, **kwargs)

    def _init(self) -> None:
        paths = ("__probe__", "auth", "info", "submit_captcha", "shutdown")
        for path in paths:
            self.app.route(f"/{path}", ["GET", "POST"] if path == "auth" else ["GET"])(
                getattr(self, path)
            )

    def response(
        self,
        *,
        ok: Union[str, dict] = "",
        error: Union[str, dict] = "",
        code: int = 200,
    ) -> Response:
        # fmt: off
        return (
            jsonify({
                "status": "Ok" if ok else error,
                "data": ok if ok else error
            }),
            code,
        )
        # fmt: on

    async def __probe__(self) -> Response:
        """
        Always returns HTTP 202 Accepted.

        :return: OK/username, 202
        """
        payload = (
            {"username": self.meta.login}
            if getattr(self.meta, "login") is not None
            else {}
        )
        return self.response(ok=payload, code=202)

    async def info(self) -> Response:
        return self.response(ok={"status": self.status.value}, code=200)

    async def auth(self) -> Response:
        if not self.status.is_waiting_for_request():
            return self.response(ok={"status": self.status.value}, code=202)

        form = await request.form
        username = form.get("username")
        password = form.get("password")

        if username is None and self.meta.login is None:
            return self.response(error={"error": "Expected a username"}, code=400)

        if password is None:
            return self.response(error={"error": "Expected a password"}, code=400)

        self.meta.login = username or self.meta.login
        self.meta.password = password

        logger.debug(
            "Setting .status to Ongoing and releasing the authentication thread lock..."
        )

        # Prohibit the server from accepting new authentication requests
        self.status.set_ongoing()

        # Unblock the authentication thread
        self.lock.release()

        return self.response(ok={"status": self.status.value}, code=200)

    async def submit_captcha(self):
        if not self.status.has_pending_captcha():
            return self.response(error={"status": self.status.value}, code=400)

        solution = request.args.get("solution")
        if solution is None:
            return self.response(
                error={"status": self.status.value, "path": self.status.captcha_path},
                code=428,
            )

        self.status.captcha_solution = solution
        self.status.set_ongoing()
        self.lock.release()
        return self.response(ok={"status": self.status.value}, code=200)

    async def shutdown(self) -> None:
        """
        Shutdowns the server.

        :raises: :exc:`Shutdown`
        """
        logger.debug("Received a shutdown request.")
        await self.app.shutdown()
        raise KeyboardInterrupt
