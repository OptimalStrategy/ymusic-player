import abc
from contextlib import suppress
from enum import Enum
from dataclasses import dataclass
from typing import Callable, Any

import requests
from loguru import logger
from selenium.webdriver.remote.webelement import WebElement
from selenium.common.exceptions import (
    TimeoutException,
    NoSuchElementException,
    NoSuchWindowException,
    WebDriverException,
    StaleElementReferenceException,
)

from ymusic.config import TMPDIR
from ymusic.driver import DriverWrapper


class AuthKind(Enum):
    """
    The possible authentication types.
    """

    #: The extended authentication form.
    FULL = 0
    #: The short authentication form (2FA).
    SHORT = 1
    #: The authentication type is either not known yet or is actually unknown.
    UNKNOWN = 2


@dataclass(repr=False)
class AuthMeta(object):
    """
    Contains the meta information associated with the current authentication.
    """

    window: str
    status: bool
    auth_type: AuthKind
    login: str = None
    password: str = None

    @property
    def success(self) -> bool:
        """
        Returns `True` if the authentication was successful.

        :return: whether the authentication is successful
        """
        return self.status

    @property
    def is_full_auth(self) -> bool:
        return self.auth_type == AuthKind.FULL


class AbstractAuth(metaclass=abc.ABCMeta):
    """
    The base class for all authenticators.

    Derived classes must implement the :code:`_authenticate()` and :code:`handle_captcha()` methods, which will be called
    if the user is not logged into the site.
    """

    def __init__(self, wrapper: DriverWrapper, **kwargs):
        self.wrapper = wrapper
        self.meta: AuthMeta = None

    def authenticate(self, login: str = None, password: str = None) -> AuthMeta:
        """
        Authenticates the user.

        :param login: optional login (user will be be prompted if missing)
        :param password: optional password (user will be prompted if missing or invalid)
        :return: an :class:`AuthMeta` instance
        """
        self.meta = self._pre_auth()
        self.meta.login = login
        self.meta.password = password

        if not self.meta.success:
            logger.debug("Unauthorized. Trying to log on...")
            self._determine_auth_type()
            self._authenticate()

        self._post_auth()
        return self.meta

    @abc.abstractmethod
    def _authenticate(self) -> None:
        """
        Method that implements the authentication.

        :return: None
        """

    @abc.abstractmethod
    def handle_captcha(self, element: WebElement) -> Any:
        """
        Downloads the captcha. Must be overridden in derived classes to actually handle the captcha.

        :return: path to the captcha file
        """
        logger.debug("Saving captcha to the temp folder...")

        captcha = element.get_attribute("src")
        filename = TMPDIR.joinpath(f"captcha.gif")

        r = requests.get(captcha)
        with filename.open("wb") as f:
            f.write(r.content)

        return str(filename)

    def _pre_auth(self) -> "AuthMeta":
        """
        Collects the meta information about the authentication process and handles popups.
        This function is called before :code:`_authenticate()`.
        """
        # We assume that the user is already authorized.
        meta = AuthMeta(
            self.wrapper.driver.current_window_handle, True, AuthKind.UNKNOWN
        )

        captcha = self.wrapper.check_class("form__captcha")
        if captcha:
            logger.debug("Detected a captcha")
            self.handle_captcha(captcha)

        gdpr_popup_btn = self.wrapper.check_class("gdpr-popup__button", timeout=3)
        if gdpr_popup_btn is not None:
            gdpr_popup_btn.click()

        ad_popup_btn = self.wrapper.check_class("payment-popup__close")
        if ad_popup_btn:
            ad_popup_btn.click()

        if self.wrapper.is_logged_in(5):
            return meta

        # Firstly we locate the log-in button and click on it.
        # This should open a tab with the title == `Authorization`
        login_btn = self.wrapper.driver.find_element_by_class_name("log-in")
        login_btn.click()

        # Switch to the authorization tab
        self.wrapper.switch_to("Authorization")

        # Check if any of the following elements exist (these are the account buttons).
        link = self.wrapper.check_class("AuthAccountListItem_default")

        if not link:
            meta.status = False
            return meta

        try:
            link.click()
        except WebDriverException as e:
            if "unknown error" not in str(e):
                raise
            logger.error("An unknown error has occurred: {}", e)
        return meta

    def _post_auth(self) -> None:
        """
        Switches the driver to the initial page and sets the :code:`status` flag on the :code:`meta` attribute to :code:`True`.
        This function is called at the end of authentication.
        """
        # Wait a few seconds before the authentication succeeds, then switch to the main tab.
        with suppress(NoSuchWindowException):
            self.wrapper.wait().until_not(
                lambda x: x.find_element_by_class_name("passport-Button")
            )
        try:
            self.wrapper.driver.close()
        except WebDriverException as e:
            logger.error("Failed to close the auth tab: {}", e)

        self.wrapper.switch(self.meta.window)

        # Check for the "yandex music is not available in your country" popup
        popup = self.wrapper.check_class("d-unavaliable__close")
        try:
            popup.click()
        except (WebDriverException, AttributeError):
            pass

        self.meta.status = True

    def _determine_auth_type(self) -> None:
        """
        Determines the authentication type.
        """

        # There are two log-in forms:
        #   full  - contains both the login and the password fields
        #   short - contains only the login field

        auth_type = AuthKind.FULL
        try:
            # the short form should contain an input element with the id below
            self.wrapper.wait().until(
                lambda x: x.find_element_by_id("passp-field-login")
            )
            auth_type = AuthKind.SHORT
        except (TimeoutException, NoSuchElementException):
            pass

        self.meta.auth_type = auth_type


class CaptchaDetected(Exception):
    def __init__(self):
        super().__init__("Captcha handling is not implemented (maybe yet)")


def recursive_short_form_auth(
    wrapper: DriverWrapper,
    login: str,
    password_provider: Callable[[], str],
    depth: int = 0,
    max_depth: int = 5,
) -> None:
    """
    Recursively attempts to log on to the site.

    :param wrapper: a DriverWrapper instance
    :param login: user's login
    :param max_depth: legacy name. corresponds to the maximum number of password attempts
    :return: None
    """
    # XXX: this check is technically not needed
    if depth >= max_depth:
        raise FailedAuthenticationError(max_depth)

    if wrapper.check_class("passp-captcha-field"):
        raise CaptchaDetected

    # Get and clear the login field in case it already contains some text
    login_field = wrapper.driver.find_element_by_id("passp-field-login")
    login_field.clear()
    login_field.send_keys(login)

    # Click on the `log-in` button.
    button = wrapper.driver.find_element_by_class_name("Button2_type_submit")
    button.click()

    # TODO: add QR code support
    # Skip the QR code if prompted
    try:
        code = wrapper.check_class("Link_view_default", 3)
        code.click()
    except StaleElementReferenceException:
        code = wrapper.check_class("Link_view_default", 3)
        code.click()
    except AttributeError:
        pass

    n_attempts = 0
    while True:
        try:
            if n_attempts >= max_depth:
                raise FailedAuthenticationError(max_depth)

            # Prompt the user for the password, then fill out the field and click on the button.
            password_field = wrapper.wait().until(
                lambda x: x.find_element_by_id("passp-field-passwd")
            )
            password = password_provider()
            password_field.send_keys(password)
            button = wrapper.check_class("Button2_type_submit")
            button.click()

            # We have successfully logged on, break out of the loop
            error = wrapper.check_class("Textinput-Hint_state_error", 3)
            if not error or not error.is_displayed():
                break

            # Repeat the steps above if the attempt has failed
            logger.debug("Invalid password! Repeating the operation.")
            print("Invalid password!")

            depth += 1
        except (NoSuchElementException, NoSuchWindowException, TimeoutException):
            break

    logger.debug("The driver appears to be logged in successfully.")


class FailedAuthenticationError(Exception):
    def __init__(self, n_attempts: int):
        super().__init__(f"The client was unable to log in {n_attempts} attempts.")


def recursive_full_form_auth(
    wrapper: DriverWrapper,
    login: str,
    password_provider: Callable[[], str],
    depth: int = 0,
    max_depth: int = 3,
) -> None:
    """
    Recursively attempts to log on to the site.

    :param wrapper: a :class:`DriverWrapper` instance.
    :param login: user's login
    :return: None
    """
    if depth >= max_depth:
        raise FailedAuthenticationError(max_depth)

    if wrapper.check_class("passp-captcha-field"):
        raise CaptchaDetected

    login_field = wrapper.driver.find_element_by_name("login")
    login_field.send_keys(login)

    # Prompt the user for the password, then fill out the field and click on the button.
    password_field = wrapper.driver.find_element_by_name("passwd")
    password = password_provider()
    password_field.send_keys(password)

    button = wrapper.driver.find_element_by_class_name("passport-Button")
    button.click()

    try:
        # Try to locate an error message
        error = wrapper.wait().until(
            lambda x: x.find_element_by_class_name("passport-Domik-Form-Error")
        )

        # Avoid artifacts from the previous calls
        if not error.is_displayed():
            return

        logger.debug("Invalid password!")
        print("Invalid password!")

        # Restore the previous internal state by going back
        get_back = wrapper.driver.find_element_by_class_name("passport-Domik-Return")
        get_back.click()

        # Repeat the steps above
        return recursive_full_form_auth(
            wrapper, login, password_provider, depth + 1, max_depth
        )
    except (NoSuchElementException, NoSuchWindowException, TimeoutException):
        pass

    logger.debug("The driver appears to be logged in successfully.")
