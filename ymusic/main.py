import json
from contextlib import suppress

import click
from loguru import logger

from ymusic.config import *
from ymusic.auth import do_auth
from ymusic.driver import create_wrapper
from ymusic.debug import repl
from ymusic.yplayer import Player, UnableToPlayContentException
from ymusic.yserver import YServer


try:
    from ymu_tray import tray_app
except ImportError:
    tray_app = None


@click.command()
@click.argument("what", required=False)
@click.option(
    "--play",
    default="radio",
    show_default=True,
    help="Play a radio, an album or a playlist.",
    autocompletion=lambda *_, incomplete: list(
        filter(lambda x: incomplete in x, ("radio", "album", "playlist"))
    ),
    type=click.Choice(["radio", "album", "playlist"], case_sensitive=False),
)
@click.option(
    "-d",
    "--driver",
    default=None,
    show_default=True,
    help="Path to your chromedriver.",
    type=click.STRING,
)
@click.option(
    "-l",
    "--login",
    default=None,
    show_default=True,
    help="Yandex account login or a phone number. You will be prompted if not provided.",
    type=click.STRING,
)
@click.option("--ignore-config", is_flag=True)
@click.option(
    "-c",
    "--config",
    default=str(CONFIG_DIR.joinpath(CONFIG_NAME)),
    show_default=True,
    help="Read settings from a config",
    type=click.Path(exists=True, dir_okay=False),
)
@click.option(
    "--cookies/--no-cookies",
    "use_cookies",
    default=True,
    show_default=True,
    help="Use cookies saved at the end of the previous session.",
)
@click.option(
    "--discard-cookies",
    is_flag=True,
    help="Don't save cookies at the end of the session.",
)
@click.option(
    "--disable-notifications",
    is_flag=True,
    help="Display notifications through a dbus service.",
)
@click.option(
    "--headless/--windowed",
    is_flag=True,
    default=True,
    show_default=True,
    help="Launch the driver in headless or windowed mode",
)
@click.option(
    "--remote-auth",
    is_flag=True,
    help="Use remote authentication. The authentication server will be "
    "launched on the same address as the player server. [NOT IMPLEMENTED]",
)
@click.option(
    "--repl", "launch_repl", is_flag=True, help="Launch REPL after authentication."
)
@click.option(
    "--start-tray",
    "start_tray",
    is_flag=True,
    help="Launch the tray app if it is available.",
)
@logger.catch(exception=Exception)
@PlayerConfig.from_args
def main(config: PlayerConfig):
    """
    Launch the player server or the debug REPL.
    Settings provided as argument override the config.

    Argument `what` is an index/id or a url of a radio, an album or a playlist you want to play.
    By default `what` is 1, meaning the first radio station. There are no default values for 'album' and 'playlist'.
    """
    if config.play in ("album", "playlist") and config.what is None:
        click.echo(
            click.style("Missing an album ID or a playlist link.", fg="red"), err=True
        )
        return
    elif config.play == "radio":
        try:
            config.what = int(config.what or 1)
        except (ValueError, TypeError):
            click.echo(
                click.style(
                    f"Radio index must be a positive integer, got '{config.what}'.",
                    fg="red",
                ),
                err=True,
            )
            return

    logger.debug("Playing {kind} '{value}'.", kind=config.play, value=config.what)

    if config.use_cookies:
        logger.info("Using cookies.")
        cookies = json.load(COOKIES.open()) if COOKIES.exists() else []
    else:
        logger.info("Ignoring cookies.")
        cookies = []

    wrapper = create_wrapper(config.driver, config.headless, LOGDIR)
    with cleanup(), wrapper.graceful_shutdown(
        save_cookies=config.save_cookies, history=config.history
    ) as driver:
        driver.connect(cookies)

        do_auth(driver, config.login, None, config.remote_auth, **config.server)

        player = Player(driver, config.enable_notifications)
        driver.set_song_history_ref(player.get_song_history_ref())

        if config.play == "radio":
            player.play_radio(config.what - 1)
        elif config.play == "album":
            player.play_album(config.what)
        else:
            player.play_playlist(config.what)

        player.turn_on_high_quality()

        volume = config.default_volume
        if volume:
            player.volume = volume

        logger.debug("Initial state:\n{}", player.page_state())

        if config.launch_repl:
            logger.info("Launching the REPL...")
            repl(driver, player)
        else:
            logger.info("Starting the server...")
            server = YServer(player, **config.server)
            server.run(debug=True)


def guarded_main():
    setup()
    with suppress(KeyboardInterrupt, UnableToPlayContentException):
        main()


__all__ = ["main", "guarded_main"]
