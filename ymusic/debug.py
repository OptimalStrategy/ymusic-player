import abc
import code
from typing import Any, Set, List, Union

from selenium.common.exceptions import NoSuchElementException, WebDriverException

from ymusic.driver import DriverWrapper
from ymusic.yplayer import Player


class Pool(dict):
    """
    A class to store and manage repl commands. Also used to expose the commands to the REPL.
    """

    def __init__(self, wrapper: DriverWrapper, player: Player):
        super().__init__()
        self.commands = set()
        self["wrapper"] = wrapper
        self["player"] = player

    def add(self, *commands: Union["Command", "type(Command)"]) -> None:
        """
        Adds the provided commands to the pool. A command may be a Command instance or a Command subclass.

        :param commands: the list of commands to add to the pool
        :return:
        """
        for cmd in commands:
            if isinstance(cmd, type):
                cmd = cmd(self)
            self[cmd.__name__] = cmd
            self.commands.add(cmd)


class Command(object, metaclass=abc.ABCMeta):
    """
    Implements a debug REPL command.
    """

    def __init__(self, pool: Pool):
        self.pool = pool
        self.pool.add(self)

    @staticmethod
    def _list_commands(cls: type) -> Set["type(Command)"]:
        """
        Lists the subclasses of the given class.

        :param cls: the class to find subclasses of
        :return: the set of cls's subclasses
        """
        return set(cls.__subclasses__()).union(
            [s for c in cls.__subclasses__() for s in Command._list_commands(c)]
        )

    @staticmethod
    def list_commands() -> List["type(Command)"]:
        """
        Returns the list of all subclasses the Command class,
        except Command itself and the DriverCommand class.
        """
        return list(Command._list_commands(Command) - {Command, DriverCommand})

    @property
    @abc.abstractmethod
    def __name__(self) -> str:
        """
        Returns the name of the command. This name will be used in the repl.

        :return: the name of the command
        """
        raise NotImplementedError

    @abc.abstractmethod
    def __call__(self, *args, **kwargs) -> Any:
        """
        Executes the command.
        """
        pass

    @abc.abstractmethod
    def __str__(self) -> str:
        """
        Returns the command's description

        :return: command description
        """
        pass

    def __repr__(self) -> str:
        """
        Alias for :func:`__str__()`.

        :return: :code:`str(self)`
        """
        return str(self)


class Quit(Command):
    """
    Implements the :code:`q` command.
    Raises :exc:`SystemExit` when called.
    """

    __name__ = "q"

    def __call__(self, *args, **kwargs) -> None:
        """

        :raises: :exc:`SystemExit`
        """
        raise SystemExit

    def __str__(self) -> str:
        return "quit"


class Help(Command):
    """
    Implements the :code:`msg` command.

    Lists the descriptions of all commands in the pool.
    """

    __name__ = "msg"

    def __init__(self, pool: Pool):
        super().__init__(pool)
        self.pool = pool

    def __call__(self, *args, **kwargs) -> None:
        """
        Displays the descriptions of all commands in the pool.
        """
        max_len = max(map(lambda c: len(c.__name__), self.pool.commands))
        for cmd in self.pool.commands:
            print(f"{cmd.__name__.ljust(max_len, ' ')} - {cmd}")

    def __str__(self) -> str:
        return "see this message"


class DriverCommand(Command):
    @property
    def wrapper(self) -> DriverWrapper:
        return self.pool["wrapper"]

    @property
    def player(self) -> Player:
        return self.pool["player"]


class SaveScreenshot(DriverCommand):
    """
    Implements the :code:`screen` command.

    Makes a screenshot using
    :meth:`DriverWrapper.take_screenshot() <ymusic.wrapper.DriverWrapper.take_screenshot>`.
    """

    __name__ = "screen"

    def __call__(self, *args) -> None:
        """
        Calls :code:`wrapper.take_screenshot()`.

        :param wrapper: a :class:`DriverWrapper` instance
        :return: None
        """
        self.wrapper.take_screenshot()

    def __str__(self):
        return "save a screenshot"


class SavePage(DriverCommand):
    """
    Implements the :code:`page` command.

    Saves a web page using
    :meth:`DriverWrapper.save_page() <ymusic.wrapper.DriverWrapper.save_page>`.
    """

    __name__ = "page"

    def __call__(self, *args) -> None:
        self.wrapper.save_page()

    def __str__(self) -> str:
        return "save the current page"


class FindEelementBy(DriverCommand):
    """
    Implements the :code:`find` command.

    Finds an element on the page and prints its (id, text, is_displayed).
    """

    __name__ = "find"

    def __call__(self, by: str, query: str, *args):
        """
        Finds an element on the page using :code:`query` and the method specified with :code:`by`.

        :param by: the search method {class, xpath, id}
        :param query: the query (e.g. class name, id, xpath string)
        """
        try:
            if by == "class":
                el = self.wrapper.driver.find_element_by_class_name(query)
            elif by == "xpath":
                el = self.wrapper.driver.find_element_by_xpath(query)
            else:
                el = self.wrapper.driver.find_element_by_id(query)
            print(f"Found: {el.id} [text = {el.text}, displayed = {el.is_displayed()}]")
        except NoSuchElementException:
            print(f"Element with {by} '{query}' does not exist")

    def __str__(self) -> str:
        return "find an element by {class, id, xpath}. ex: find('class', 'button')"


class ClickOn(DriverCommand):
    """
    Implements the :code:`click` command.

    Finds an element on the page and attempts to click on it.
    """

    __name__ = "click"

    def __call__(self, by: str, query: str, *args):
        """
         Finds an element on the page using :code:`query` and the method with :code:`by`, then clicks on it.


        :param by: the search method {class, xpath, id}
        :param query: the query (e.g. class name, id, xpath string)
        """
        try:
            if by == "class":
                el = self.wrapper.driver.find_element_by_class_name(query)
            elif by == "xpath":
                el = self.wrapper.driver.find_element_by_xpath(query)
            else:
                el = self.wrapper.driver.find_element_by_id(query)
            try:
                el.click()
                print(f"Clicked: {el.text}")
            except WebDriverException as e:
                print(f"Unable to click on the element with {by} '{query}': {e}")
        except NoSuchElementException:
            print(f"Element with {by} '{query}' does not exist")

    def __str__(self) -> str:
        return "click on an element found by {class, id, xpath}. ex: click('class', 'button')"


class TurnOnHq(DriverCommand):
    """
    Switches the quality to High.
    """

    __name__ = "hqon"

    def __call__(self, *args, **kwargs):
        """
        Switches the quality to high.
        """
        self.player.turn_on_high_quality()

    def __str__(self) -> str:
        return "turn on high quality"


class TurnOffHq(DriverCommand):
    """
    Switches the quality to Low.
    """

    __name__ = "hqoff"

    def __call__(self, *args, **kwargs):
        """
        Switches the quality to low.
        """
        self.player.turn_off_high_quality()

    def __str__(self) -> str:
        return "turn off high quality"


class Play(DriverCommand):
    """
    Resumes the current track.
    """

    __name__ = "play"

    def __call__(self, *args, **kwargs):
        """
        Resumes the current track.
        """
        self.player.play()

    def __str__(self) -> str:
        return "resume the music"


class Pause(DriverCommand):
    """
    Pauses the current track.
    """

    __name__ = "pause"

    def __call__(self, *args, **kwargs):
        """
        Pauses the current track.
        """
        self.player.pause()

    def __str__(self) -> str:
        return "pause the music"


class Skip(DriverCommand):
    """
    Skips the current track.
    """

    __name__ = "skip"

    def __call__(self, *args, **kwargs):
        """
        Skips the current track.
        """
        self.player.skip()

    def __str__(self) -> str:
        return "skip the current track"


class Like(DriverCommand):
    """
    Adds the current track to favorites.
    """

    __name__ = "like"

    def __call__(self, *args, **kwargs):
        """
        Adds the current track to favorites.
        """
        self.player.like()

    def __str__(self) -> str:
        return "add the current track to favorites"


def _repl(wrapper: DriverWrapper, player, commands: List[Command]) -> None:
    """
    Starts an interactive REPL session. Raises :exc:`SystemExit` when received the stop signal.

    :param wrapper: a :class:`DriverWrapper` instance
    :param commands: the list of commands to add to the pool
    :return: None
    """
    pool = Pool(wrapper, player)
    pool.add(*commands)

    console = code.InteractiveConsole(locals=pool, filename="<debug>")
    try:
        console.interact(banner=f"Type q() to quit, msg() to see the list of commands")
    except SystemExit:
        pass


def repl(wrapper: DriverWrapper, player: Player):
    """
    Launches an interactive REPL that forwards simple queries to the driver.

    :param wrapper: a :class:`DriverWrapper`
    :return: None
    """
    _repl(wrapper, player, Command.list_commands())
