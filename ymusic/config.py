import json
import shutil
import platform
import functools
from contextlib import contextmanager
from dataclasses import dataclass, field
from shutil import copy
from pathlib import Path
from typing import Dict, Any, Union

from loguru import logger


#: Configuration and logging directory
CONFIG_DIR = Path.home().joinpath(".yplayer").absolute().resolve()

#: Default config name
CONFIG_NAME = "yplayer.json"

#: Path to the config
CONFIG = CONFIG_DIR.joinpath(CONFIG_NAME).absolute().resolve()

COOKIES = CONFIG_DIR.joinpath(".cookies.json")

LOGDIR = CONFIG_DIR.joinpath("./logs")

logger.add(str(LOGDIR.joinpath("yplayer.log")))

DEFAULT_CONFIG = {
    "use_cookies": True,
    "save_cookies": True,
    "driver": "chromedriver",
    "notifications": True,
    "always_open_captcha_window": False,
    "auth": {"login": None, "remote_auth": False},
    "default_volume": 100,
    "server": {"host": "localhost", "port": 7447},
    "icon": str(CONFIG_DIR.joinpath("./icon.png").resolve()),
    "history": {
        "dump": True,
        "path": str(CONFIG_DIR.joinpath("history.json").resolve()),
    },
}

TMPDIR = CONFIG_DIR.joinpath(".tmp").resolve()

#: Path to the default icon
DEFAULT_ICON = Path(__file__).parent.joinpath("./binary/icon.png").absolute().resolve()
DEFAULT_ICON_WINDOWS = (
    Path(__file__).parent.joinpath("./binary/icon.ico").absolute().resolve()
)
IS_WINDOWS = platform.system() == "Windows"


def setup() -> None:
    """
    Creates the required directories and initializes the default config if needed.
    """
    if not CONFIG_DIR.exists():
        logger.debug(f"Creating config directory: {CONFIG_DIR}")
        CONFIG_DIR.mkdir()

    config = CONFIG
    if not config.exists():
        logger.debug("Initialized the default config")
        json.dump(DEFAULT_CONFIG, config.open("w"), indent=4)

    if not LOGDIR.exists():
        logger.debug("Initialized the logging directory")
        LOGDIR.mkdir()

    cfg = json.load(config.open("r"))
    icon = Path(cfg.get("icon", DEFAULT_CONFIG["icon"])).absolute().resolve()
    if not icon.exists():
        copy(DEFAULT_ICON, icon)
        logger.debug("Copied the default icon to '{}'", icon)

    if not TMPDIR.exists():
        logger.debug("Initialized the temp directory")
        TMPDIR.mkdir()


@logger.catch
def _clear_tmp():
    logger.debug("Cleaning the temporary directory...")
    shutil.rmtree(TMPDIR)


@contextmanager
def cleanup():
    try:
        yield
    finally:
        _clear_tmp()


@dataclass
class PlayerConfig(object):
    what: Union[str, int]
    play: str
    driver: str
    login: str
    ignore_config: bool
    config: str
    use_cookies: bool
    discard_cookies: bool
    disable_notifications: bool
    headless: bool
    remote_auth: bool
    launch_repl: bool
    start_tray: bool
    default_volume: float = None
    always_open_captcha_window: bool = False
    icon: str = DEFAULT_CONFIG["icon"]
    server: Dict[str, Any] = field(default_factory=lambda: DEFAULT_CONFIG["server"])
    history: Dict[str, Any] = field(default_factory=lambda: DEFAULT_CONFIG["history"])

    def __post_init__(self):
        if self.ignore_config:
            return

        config = Path(self.config).absolute().resolve()
        config = json.load(config.open())

        self._replace_default(config, "driver", default="chromedriver")
        self._replace_default(config.get("auth", {}), "login")
        self._replace_default(config.get("auth", {}), "remote_auth")
        self._replace_default(config, "use_cookies")
        self._replace_default(config, "discard_cookies")
        self._replace_default(config, "disable_notifications")
        self._replace_default(config, "headless")
        self._replace_default(config, "server", {})
        self._replace_default(config, "default_volume")
        self._replace_default(config, "icon")
        self._replace_default(config, "server")
        self._replace_default(config, "history")
        self._replace_default(config, "always_open_captcha_window")

    @property
    def save_cookies(self) -> bool:
        return not self.discard_cookies

    @property
    def enable_notifications(self) -> bool:
        return not self.disable_notifications

    def _replace_default(self, config: Dict[str, Any], key: str, default: Any = None):
        value = getattr(self, key)
        setattr(
            self,
            key,
            getattr(self, key) if value is not None else config.get(key, default),
        )

    @staticmethod
    def from_args(f):
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            return f(PlayerConfig(*args, **kwargs))

        return wrapper


__all__ = [
    "CONFIG_DIR",
    "CONFIG_NAME",
    "CONFIG",
    "COOKIES",
    "LOGDIR",
    "DEFAULT_CONFIG",
    "setup",
    "cleanup",
    "PlayerConfig",
]
