from io import BytesIO
from PIL import Image


def save_image_windows(name: str, b: bytes) -> str:
    """
    Saves image as ICO on windows.

    :param name: the name of the image
    :param b: image contents
    :return:
    """
    stream = BytesIO(b)
    img = Image.open(stream)
    img.save(name)
    return name
