import os
import json
import datetime as dt
from contextlib import contextmanager
from pathlib import Path
from typing import Dict, Any, Optional, List

from loguru import logger
from selenium.webdriver import Chrome, ChromeOptions
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import (
    TimeoutException,
    NoSuchElementException,
    WebDriverException,
)
from ymusic.config import COOKIES, LOGDIR, DEFAULT_CONFIG, IS_WINDOWS

URL = "https://music.yandex.com/radio"


@contextmanager
def patched_popen() -> None:
    """
    By default, ChromeDriver tracks the SIG* signals, shutting itself down when received SIGINT.
    Which is bad, since we cannot save the cookies and do other shutdown-related things if the driver is not up.

    This context manager replaces subprocess.Popen with FakePopen, which
    provides os.setgprp() as preexec_fn on Linux.

    .. code-block:: python

        # the driver will ignore ctrl + c
        with patched_popen():
            driver = create_driver("chromedriver", False)

    :yield: None
    """
    import os
    import subprocess

    Popen = subprocess.Popen

    class FakePopen(Popen):
        def __init__(self, *args, **kwargs):
            if not IS_WINDOWS:
                kwargs["preexec_fn"] = os.setpgrp

            super().__init__(*args, **kwargs)

    subprocess.Popen = FakePopen
    try:
        yield
    finally:
        subprocess.Popen = Popen


class DriverWrapper(object):
    """
    A wrapper over selenium's ChromeDriver that provides several helper & management functions.
    """

    def __init__(self, driver: Chrome, logdir: Path = None):
        """
        :param driver: a Chrome instance
        """
        self._driver = driver
        self._screenshot_id = 0
        self._page_id = 0
        self.logdir = logdir or Path("./logs")
        self._song_history_ref: Optional[List[str]] = None

    @property
    def driver(self) -> Chrome:
        """
        Returns the saved driver.

        :return: the saved driver
        """
        return self._driver

    @property
    def cookies(self) -> List[Dict[str, Any]]:
        """
        Returns the cookies associated with the currently opened page.

        :return: a dict of cookies
        """
        return self.driver.get_cookies()

    def connect(self, cookies: List[Dict[str, Any]]) -> None:
        """
        Opens yandex.radio and adds the given cookies to the driver.

        :param cookies: the dict of cookies to add
        :return: None
        """
        self.driver.get(URL)
        for cookie in cookies:
            if isinstance(cookie.get("expiry"), float):
                cookie["expiry"] = int(cookie["expiry"])
            self.driver.add_cookie(cookie)

    def is_logged_in(self, timeout: float = 0.1) -> bool:
        """
        Returns true if the user is logged on to the site.

        :param timeout: operation timeout
        :return: is logged in
        """
        return self.check_class("head__userpic", timeout) is not None

    def switch(self, window: str):
        """
        Switches to the window with the name :code:`window`.

        :param window: the window to switch to
        :return: None
        """
        self.driver.switch_to.window(window)

    def switch_to(self, window: str):
        """
        Cycles the windows until it finds a window with the given name.

        :param window: the name of the window to switch to
        """
        for tab in self.driver.window_handles:
            self.switch(tab)
            if self.window_name == window:
                break

    @property
    def window_name(self) -> str:
        """
        Returns the name of the current window.

        :return: the name of the current window
        """
        return self.driver.title

    def wait(self, timeout: float = 3) -> WebDriverWait:
        """
        Returns :code:`WebDriverWait(driver, timeout)`

        :param timeout: the timeout in seconds
        :return: :code:`WebDriverWait` applied to the stored driver
        """
        return WebDriverWait(self.driver, timeout)

    def just_wait(self, seconds: float):
        """
        Waits :code:`seconds` seconds.

        :param seconds: the number of seconds to wait
        :return: None
        """
        try:
            WebDriverWait(self.driver, seconds).until(lambda x: None)
        except TimeoutException:
            pass

    def check_class(self, cls: str, timeout: float = 0.1) -> Optional[WebElement]:
        """
        Attempts to find an element with the class :code:`cls`.
        Returns None if no elements were found or the timeout is exceeded.

        :param cls: the class to check
        :param timeout: the timeout
        :return: the found element or None
        """
        try:
            return self.wait(timeout).until(lambda x: x.find_element_by_class_name(cls))
        except (NoSuchElementException, TimeoutException):
            return None

    def check_nth_class(
        self, cls: str, index: int = 0, timeout: float = 0.1
    ) -> Optional[WebElement]:
        """
        Attempts to find the element with the class :code:`cls` at :code:`index`.
        Returns None if the element is not found, in case the timeout is exceeded or the index is incorrect.

        :param cls: the class to check
        :param index: the index of the element
        :param timeout: the timeout
        :return: the element or None
        """
        try:
            return self.wait(timeout).until(
                lambda x: x.find_elements_by_class_name(cls)[index]
            )
        except (NoSuchElementException, TimeoutException, IndexError):
            return None

    def take_screenshot(self) -> "DriverWrapper":
        """
        Takes a screenshot and saves it to the logging directory.

        :return: self
        """
        logger.debug(f"Taking screenshot #{self._screenshot_id}")
        with self.logdir.joinpath(f"pic{self._screenshot_id}.png").open("wb") as f:
            f.write(self.driver.get_screenshot_as_png())
        self._screenshot_id += 1
        return self

    def save_page(self) -> "DriverWrapper":
        """
        Saves the currently opened web page to the logging directory.

        :return: self
        """
        logger.debug(f"Saving page #{self._page_id}")
        with self.logdir.joinpath(f"page{self._page_id}.html").open("w") as f:
            f.write(self.driver.page_source)
        self._page_id += 1
        return self

    @staticmethod
    def close_all(driver: Chrome):
        """
        Closes all windows and quits the driver afterwards.

        :param driver: the driver to be closed
        :return: None
        """
        logger.debug("Closing windows...")
        for window in driver.window_handles:
            driver.switch_to.window(window)
            logger.debug(f"Closing {driver.title}")
            try:
                driver.close()
            except WebDriverException:
                pass
        logger.debug("Shutting down the driver...")
        driver.quit()
        logger.debug("Done.")

    def close(self):
        """
        Alias for :meth:`close_all() <ymusic.aurh.DriverWrapper.close_all>` that automatically provides the driver..

        :return: None
        """
        self.close_all(self.driver)

    @contextmanager
    def graceful_shutdown(
        self, save_cookies: bool = True, history: Dict[str, Any] = None
    ) -> None:
        """
        Closes the driver, optionally saving the cookies.

        :param save_cookies: whether to save the cookies or not
        :return: self
        """
        try:
            yield self
        finally:
            if save_cookies:
                json.dump(self.cookies, COOKIES.open("w"))
                logger.debug("Saved cookies.")

            if history:
                save_song_history(history, self._song_history_ref)

            self.close()

    def set_song_history_ref(self, ref: List[str]) -> None:
        """
        Assigns the given reference to the list containing the played tracks to :code:`_song_history_ref`.

        :param ref: a reference to the song history
        :return: None
        """
        self._song_history_ref = ref


def create_driver(executable: str, headless: bool) -> Chrome:
    """
    Creates a new Chrome driver, optionally headless.

    :param executable: path to the chromedriver executable
    :param headless: whether to turn on the headless mode
    :return: a new `Chrome` instance
    """
    # Make sure that the driver directory is on PATH
    epath = Path(executable).absolute().resolve()
    if epath.exists() and str(epath.parent) not in os.environ["PATH"]:
        os.environ["PATH"] += os.pathsep + str(epath.parent)

    opts = ChromeOptions()
    if headless:
        opts.add_argument("headless")
    with patched_popen():
        driver = Chrome(executable, options=opts)
    return driver


def create_wrapper(
    executable: str, headless: bool, logdir: Path = LOGDIR
) -> DriverWrapper:
    """
    Creates a :class:`DriverWrapper` instance using the provided executable and settings.

    :param executable: path to the chromedriver executable
    :param headless: whether to turn on the headless mode
    :param logdir: path to the logging directory
    :return: the initialized :class:`DriverWrapper` instance
    """
    return DriverWrapper(create_driver(executable, headless), logdir)


@logger.catch()
def save_song_history(conf: Dict[str, Any], song_history: List[str]) -> None:
    """
    Attempts to save the list of tracks according to the config.

    :param conf: the song history configuration to follow
    :param song_history: a reference to the song history
    :return: None
    """
    dump = conf.get("dump", DEFAULT_CONFIG["history"]["dump"])
    path = Path(conf.get("path", DEFAULT_CONFIG["history"]["path"]))

    if path.is_dir():
        return logger.error(f"Song history path is a directory:\n{conf}")

    if not dump:
        return logger.debug("Song history dump is disabled.")

    if not song_history:
        return logger.debug("Song history is empty.")

    if path.exists():
        hist = json.load(open(path))
    else:
        hist = {}

    hist[dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S")] = song_history[1:]
    json.dump(hist, open(path, "w"), indent=4, sort_keys=True)

    logger.debug("Dumped song history.")
