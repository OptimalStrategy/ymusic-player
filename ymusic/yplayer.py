import re
import json
from enum import Enum
from typing import Dict, Any, Optional, List

import requests
from loguru import logger

from selenium.common.exceptions import NoSuchElementException, WebDriverException
from selenium.webdriver import ActionChains

from ymusic.driver import DriverWrapper, URL
from ymusic.config import TMPDIR, IS_WINDOWS
from ymusic.notifications import PlayerNotificationService
from ymusic.utils import save_image_windows


MUSIC_URL = re.compile(r"https://music\.yandex\.[a-z]+/.+")


class UnableToPlayContentException(BaseException):
    """
    Raised if the server is unable to play a radio, an album or a playlist.
    """

    def __init__(self, metadata: Optional[Any] = None, *args):
        super().__init__(*args)
        self.metadata = metadata


class Player(object):
    """
    Provides APIs to communicate with the web player.
    """

    _notification = PlayerNotificationService()
    _default_page_titles = set()
    _DEFAULT_YMUSIC_PREFIXES = {"Yandex Music"}

    def __init__(self, wrapper: DriverWrapper, notifications: bool = True):
        self._wrapper = wrapper

        self._station: str = None  #: station name
        self._notification.is_active = notifications
        self._notification.init(self)

        self._content_kind: ContentKind = ContentKind.UNKNOWN
        self._song_cache = "<Unknown>"
        self._is_volume_initialized = False
        self._state = self.page_state()

    @property
    def quality(self) -> str:
        """
        Returns the currently used quality.

        :return: the current quality {HQ, LQ}
        """
        return (
            "HQ" if self._wrapper.check_class("hq_active", 0.05) is not None else "LQ"
        )

    @property
    def station(self) -> str:
        """
        Returns the current station.

        :return: the current station.
        """
        return self._station

    @property
    def liked(self) -> "LikeResult":
        """
        Returns the current `like` status.

        :return: LikeResult.{ADDED, REMOVED}
        """
        result = self._wrapper.check_class("d-icon_heart-full")
        return LikeResult.ADDED if result else LikeResult.REMOVED

    @property
    def song(self) -> str:
        """
        Returns the name of the current track.

        :return: the name of the track
        """
        s = self._wrapper.driver.title
        if s in self.default_songs:
            return self._song_cache
        self._song_cache = s
        return s

    @property
    def track_url(self) -> Optional[str]:
        """
        Returns the url of the current track.

        :return: the url of the tarck
        """
        url = self._wrapper.check_nth_class("track__title", -1)
        return url.get_attribute("href") if url else None

    @property
    def default_songs(self):
        """
        A set of the names returned by .song() if nothing is playing right now.

        :return: the default values for .song()
        """
        return self._default_page_titles

    @property
    def song_history(self) -> List[str]:
        """
        Returns the history of all track played during the session.
        This property does not work with notifications disabled.

        :return: the track history
        """
        # First song is the window title.
        return self._notification.song_history[1:]

    @property
    def progress(self) -> float:
        """
        Returns the current track progress.

        :return: current progress in % or -1 if the operation has failed
        """
        bar = self._wrapper.check_class("progress__line__branding")
        if bar:
            progress = bar.get_attribute("style")

            # style = "transform: translateX(-n%);"
            return 100 + float(progress[22:-3])
        return -1

    def get_cover_url(self) -> Optional[str]:
        """
        Returns the URL of the track's album cover.

        :return: the url of the album cover
        """
        cover = self._wrapper.check_class("entity-cover__image")
        return cover.get_attribute("src").strip("/") if cover else cover

    def _save_album_cover_by_url(self, url: str) -> Optional[str]:
        """
        Downloads the cover using :code:`url`, then returns the name of the saved file.
        If the cover for this url has already been downloaded, returns the path immediately.

        Returns None if the fails to retrieve the cover.

        :param url: the url of the track's album cover
        :return: path to the cover or None
        """
        extension = ".ico" if IS_WINDOWS else ".jpg"
        filename = TMPDIR.joinpath(f"{hash(url)}.{extension}")

        if filename.exists():
            return str(filename)

        try:
            r = requests.get(url)

            if IS_WINDOWS:
                save_image_windows(str(filename), r.content)
            else:
                with filename.open("wb") as f:
                    f.write(r.content)

            return str(filename)
        except (requests.HTTPError, ConnectionError) as e:
            logger.error("Failed to download the album cover")
            logger.exception(e)

    @logger.catch
    def save_album_cover(self) -> Optional[str]:
        """
        Downloads the cover of the current track and returns the path to the saved file.
        If the cover has already been downloaded, returns the path immediately.

        Returns None if fails to retrieve the cover.

        :return: path to the cover or None
        """
        url = self.get_cover_url()

        if not url:
            return

        cover = None
        # XXX: maybe use bigger covers
        sizes = [url]  # + [url.replace("50x50", "200x200")]
        while not cover and sizes:
            cover = self._save_album_cover_by_url(sizes.pop())
        return cover

    @property
    def volume(self) -> float:
        """
        Returns the current volume.

        :return: current volume in % or -1 if failed to retrieve
        """
        parent = self._wrapper.check_class("volume__slider", 0.05)
        if parent:
            slider = self._wrapper.check_nth_class("d-slider-vert__filled", -1, 0.05)
            volume = slider.get_attribute("style")
            # style = "height: n%;",
            return float(volume[8:-2])
        return -1

    @volume.setter
    def volume(self, value: float):
        """
        Changes the volume to :code:`value`%. The scale is logarithmic.

        The following formula is used to calculate the volume:
            >>> height = 136  # slider height in pixels
            >>> volume = 50  # current volume in %
            >>> value = 75  # new volume in %
            >>> delta = lambda vol, val: (vol - val) / 100 * height
            >>> d = delta(volume, value)  # delta in pixels
            >>> d
            -34
            >>> pixels = volume / 100 * height  # current slider height
            >>> pixels -= d # applying delta
            >>> assert pixels / height == value / 100
            0.75

        :param value: new volume percentage
        :return: None
        """
        el = self._wrapper.check_class("volume__btn")
        slider = self._wrapper.check_nth_class("d-slider-vert__drag", -1)
        size = self._wrapper.check_nth_class("d-slider-vert__track", -1).size
        height = size["height"]

        volume = self.volume
        value = min(max(value, 0), 100)
        delta = volume - value / 100 * height

        move = ActionChains(self._wrapper.driver)
        move.move_to_element(el).move_to_element(slider).click_and_hold(
            slider
        ).move_by_offset(0, delta).release().perform()

        logger.debug("Changed volume to {:.2f}%.", self.volume)

        # The first attempt to set the volume usually fails
        # due to the inappropriate state of the slider.
        # The second attempt *should* be ok.
        if not self._is_volume_initialized:
            self._is_volume_initialized = True
            logger.debug("Changing volume again after the slider initialization.")
            self.volume = value

    def internal_state(self) -> Dict[str, Any]:
        """
        Returns the internally tracked state of the player.

        :return: the state of the player
        """
        return {
            "station": self.station,
            "quality": self.quality,
            "favorite": str(self.liked),
            "volume": self.volume,
            "progress": self.progress,
            "default_title": list(self._default_page_titles or [""])[0],
            "song_url": self.track_url or "<unknown>",
            "kind": str(self._content_kind),
        }

    def page_state(self) -> Dict[str, Any]:
        """
        Returns the public state of the web player + the internal state of this abstraction.

        :return: the full state of the player
        """
        body = self._wrapper.check_class("theme") or self._wrapper.check_class(
            "theme-white"
        )
        state = {}
        if body:
            state.update(json.loads(body.get_attribute("data-unity-state") or "{}"))
        state.update(self.internal_state())
        self._state = state
        return self._state

    def play_radio(self, idx: int = 0) -> bool:
        """
        Switches to radio #:code:`idx`.

        :param idx: the id of the radio to switch to
        :return: operation result
        """
        if self._wrapper.driver.current_url != URL:
            self._wrapper.driver.get(URL)

        try:
            elements = self._wrapper.wait().until(
                lambda x: x.find_elements_by_class_name("jsClickable")
            )
            # Scroll to the radio button since it may be obstructed by the footer information message.
            # (provide true if the element is beneath, false if above)
            self._wrapper.driver.execute_script(
                "arguments[0].scrollIntoView(true);", elements[idx]
            )
            elements[idx].click()

            logger.debug(f"Selected radio #{idx}: '{elements[idx].text}'")
            self._station = elements[idx].text.strip()
            self._notification.on_radio_select(self._station, idx).show()
            if self._wrapper.driver.title.startswith(
                tuple(Player._DEFAULT_YMUSIC_PREFIXES)
            ):
                Player._default_page_titles.add(self._wrapper.driver.title)
            self._content_kind = ContentKind.RADIO
            return True
        except (NoSuchElementException, IndexError) as e:
            logger.error(f"Failed to select the radio at {idx}")
            logger.exception(e)
            self._notification.on_error(
                f"Failed to select the radio #{idx + 1}.", exc=True
            ).show()
            return False

    def play_album(self, album: str) -> None:
        """
        Plays the album at :code:`album`. If :code:`album` is a numeric string,
        converts it into a valid url.

        :param album: album url or id
        :raises: :exc:`UnableToPlayContentException`
        :return: None
        """
        logger.debug("Attempting to play an album: '{album}'", album=album)

        if album.isnumeric():
            album = f"https://music.yandex.com/album/{album}"

        if not album.startswith("https://"):
            album = f"https://{album}"

        if MUSIC_URL.match(album) is None:
            raise UnableToPlayContentException(
                album, "Album urls must belong to the music domain."
            )

        r = requests.get(album)
        if r.status_code != 200:
            print(f"Album '{album}' does not exist.")
            logger.error(
                "Unable to play '{album}': album does not exist. [status = {status}]",
                album=album,
                status=r.status_code,
            )
            raise UnableToPlayContentException(album, "Album does not exist.")

        self._wrapper.driver.get(album)
        self._wrapper.driver.execute_script("window.scrollBy(0, 250);")

        play_btn = self._wrapper.check_nth_class("button-play__type_album", timeout=1)
        if play_btn:
            try:
                play_btn.click()
            except WebDriverException:
                pass
        else:
            print(f"Failed to play album '{album}'.")
            logger.error(
                "Failed to play album '{album}'. Play button is not available.",
                album=album,
            )
            self._notification.on_error("Failed to play the album.").show()
            raise UnableToPlayContentException(album, "Play button is not available.")

        self._content_kind = ContentKind.ALBUM

    def play_playlist(self, playlist: str) -> None:
        """
        Plays the playlist at :code:`playlist`.

        :param playlist: playlist url
        :raises: :exc:`UnableToPlayContentException`
        :return: None
        """
        logger.debug("Attempting to play a playlist: '{playlist}'", playlist=playlist)

        if not playlist.startswith("https://"):
            playlist = f"https://{playlist}"

        if MUSIC_URL.match(playlist) is None:
            raise UnableToPlayContentException(
                playlist, "Playlist urls must belong to the music domain."
            )

        self._wrapper.driver.get(playlist)
        self._wrapper.driver.execute_script("window.scrollBy(0, 250);")

        play_btn = self._wrapper.check_class("button-play__type_playlist")
        if play_btn:
            try:
                play_btn.click()
            except WebDriverException:
                pass
        else:
            print(
                f"Failed to play '{playlist}'. "
                f"Please check that the playlist exists and you have the permissions required to access it."
            )
            logger.error(
                "Failed to play playlist '{playlist}': play button is not available.",
                playlist=playlist,
            )
            self._notification.on_error("Failed to play the playlist").show()
            raise UnableToPlayContentException(
                playlist,
                "Please check that the playlist exists and you have the permissions required to access it.",
            )

        self._content_kind = ContentKind.PLAYLIST

    def play_track(self, track: str) -> None:
        """
        Plays the track at :code:`track:`.

        :param track: track url
        :raises: :exc:`UnableToPlayContentException`
        :return: None
        """
        logger.debug("Attempting to play a track: '{track}'", track=track)

        if not track.startswith("https://"):
            track = f"https://{track}"

        if MUSIC_URL.match(track) is None:
            raise UnableToPlayContentException(
                track, "Track urls must belong to the music domain."
            )

        self._wrapper.driver.get(track)

        element = self._wrapper.check_class("d-track_selected")
        try:
            button = element.find_element_by_class_name("button-play__type_track")
            ActionChains(self._wrapper.driver).move_to_element(element).move_to_element(
                button
            ).click().perform()
        except (AttributeError, NoSuchElementException) as e:
            print(f"Failed to play track '{track}'.")
            logger.error("Failed to play track '{track}'.", track=track)
            logger.exception(e)
            self._notification.on_error("Failed to play the track").show()
            raise UnableToPlayContentException(track, "Failed to play the track")

        # Tracks cannot exist without an album.
        # Might add a stop-signal in later though.  XXX: What?
        self._content_kind = ContentKind.ALBUM

    def play(self) -> bool:
        """
        Starts playing the track if possible.

        :return: true if succeed
        """
        button = self._wrapper.check_class("player-controls__btn_play", 0.05)
        if button and button.get_attribute("title") == "Play [P]":
            button.click()
            logger.debug("Pressed play")
            self._notification.on_play(self.song, self.save_album_cover()).show()
            return True
        return False

    def pause(self) -> bool:
        """
        Pauses the track if possible.

        :return: true if succeed
        """
        button = self._wrapper.check_class("player-controls__btn_pause", 0.05)
        if button:
            button.click()
            logger.debug("Pressed pause")
            # Pressing pause always stops the track
            return True
        return False

    def back(self) -> bool:
        """
        Returns to the previous track.

        :return: true if succeed
        """
        button = self._wrapper.check_class("d-icon_track-prev", 0.05)
        if button:
            button.click()
            logger.debug("Switched to the previous track ({})", self.song)
            return True
        return False

    def skip(self) -> bool:
        """
        Skips the track if possible.

        :return: true if succeed
        """
        button = self._wrapper.check_class("d-icon_track-next", 0.05)
        if button:
            button.click()
            logger.debug("Skipped the current track ({})", self.song)
            return True
        return False

    def like(self) -> "LikeResult":
        """
        Adds the current track to or removes from favorites.

        :return: true if succeed
        """
        element = self._wrapper.check_class("d-like_theme-player")
        if element:
            try:
                element.click()
            # Each click on `Add to favourites` leads to the following selenium exception:
            # [... element is not attached to the page document]
            # So we just ignore it
            except WebDriverException:
                pass
            finally:
                result = self.liked
                song = self.song
                logger.debug(
                    "Added the track to favorites ({})"
                    if result == LikeResult.ADDED
                    else "Removed the track from favorites ({})",
                    song,
                )
                self._notification.on_like(result, song, self.save_album_cover()).show()
                return result
        return LikeResult.FAIL

    def turn_on_high_quality(self, delay: float = 0.05, timeout: float = 10) -> bool:
        """
        Turns on high quality if possible.

        :param delay: delay of the HQ icon check
        :param timeout: timeout of the operation
        :return: status of the operation
        """
        if self._wrapper.check_class("hq_active", delay) is None:
            self._wrapper.wait(timeout).until(
                lambda x: x.find_element_by_class_name("hq__icon").is_displayed()
            )
            self._wrapper.driver.find_element_by_class_name("hq__icon").click()
            logger.debug("Switched to HQ")
            return True
        return False

    def turn_off_high_quality(self, delay: float = 0.05, timeout: float = 10) -> bool:
        """
        Turns off high quality if possible.

        :param delay: delay of the HQ icon check
        :param timeout: timeout of the operation
        :return: status of the operation
        """
        if self._wrapper.check_class("hq_active", delay) is not None:
            self._wrapper.wait(timeout).until(
                lambda x: x.find_element_by_class_name("hq_active").is_displayed()
            )
            self._wrapper.driver.find_element_by_class_name("hq_active").click()
            logger.debug("Switched to LQ")
            return True
        return False

    def disable(self) -> None:
        """
        Disables notifications.

        :return: None
        """
        self._notification.is_active = False

    def get_song_history_ref(self) -> List[str]:
        """
        Returns a reference to the song history stored in the `_notification` field.

        :return: a reference to the song history
        """
        return self._notification.song_history


class LikeResult(Enum):
    """
    Represents the :code:`like` operation result.
    """

    FAIL = 0
    REMOVED = 1
    ADDED = 2

    def is_ok(self) -> bool:
        """
        Returns True if the instance is not Fail.

        :return: self != Fail
        """
        return self != self.FAIL


class ContentKind(Enum):
    """
    Represents the content kind.
    """

    RADIO = 0
    ALBUM = 1
    PLAYLIST = 2
    UNKNOWN = -1
