from logging.config import dictConfig
from typing import Tuple, Any, Optional

from loguru import logger
from quart import Quart, jsonify, request
from ymusic.yplayer import Player, UnableToPlayContentException


# Set server logs to ERROR
dictConfig({"version": 1, "loggers": {"quart.serving": {"level": "ERROR"}}})

RESPONSE_OK = "Ok"
RESPONSE_ERR = "Err"


def maybe_float(value: str) -> Optional[float]:
    """
    Converts the value to float.
    Returns float(value) on success, None otherwise.

    :param value: value
    :return: float(value) or None
    """
    try:
        return float(value)
    except (ValueError, TypeError):
        return None


class YServer(object):
    """
    The main server. Exposes player's APIs over HTTP.
    """

    app = Quart(__name__)
    Response = Tuple[str, int]

    def __init__(self, player: Player, host: str = "localhost", port: int = 7447):
        """

        :param player: Player instance
        :param host: server host
        :param port: server port
        """
        self.host = host
        self.port = port
        self.player = player

        self._init()

    def run(self, *args, **kwargs) -> None:
        """
        Starts the server.

        """
        try:
            self.app.run(self.host, self.port, use_reloader=False, *args, **kwargs)
        except KeyboardInterrupt:
            pass

    def unwrap(
        self, result: bool, data: Any = None, error_status: Optional[int] = 400
    ) -> Response:
        """
        Returns HTTP 200 or HTTP `error_status` with a json built according to the provided arguments.

        :param result: request status
        :param data: optional data
        :return: json, status
        """
        status = 200 if result else error_status
        return (
            jsonify(
                {"status": RESPONSE_OK if result else RESPONSE_ERR, "data": data or {}}
            ),
            status,
        )

    def _init(self) -> None:
        """
        Adds the API methods below to the server.

        :return: None
        """
        methods = (
            "status",
            "play_radio",
            "play_album",
            "play_playlist",
            "play_track",
            "song_history",
            "volume",
            "cover",
            "song",
            "play",
            "pause",
            "skip",
            "back",
            "like",
            "hq_on",
            "hq_off",
            "__heartbeat__",
            "shutdown",
        )
        for method in methods:
            self.app.route(f"/{method}")(getattr(self, method))

    async def __heartbeat__(self) -> Response:
        """
        Always returns HTTP 200 OK.

        :return: OK, 200
        """
        return "OK", 200

    async def status(self) -> Response:
        """
        Returns the internal + external states of the player.
        """
        return self.unwrap(True, data=self.player.page_state())

    async def play_radio(self) -> Response:
        """
        Plays the station at an index.

        endpoint /play_radio
        @param ?index: <int> = 0 - the index of a radio station
        """
        index: str = request.args.get("index", "0")
        if not index.isnumeric():
            return self.unwrap(
                False,
                data={
                    "error": f"Bad station index: expected a number, got {index}.",
                    "error_meta": index,
                },
            )
        res = self.player.play_radio(int(index))
        return self.unwrap(res, data=self.player.page_state())

    async def play_album(self) -> Response:
        """
        Plays the album with with an id or at a link.

        endpoint /play_album
        @param ?aid: <int>  - album id
        @param ?link: <str> - album link
        """
        aid: Optional[str] = request.args.get("id")
        link: Optional[str] = request.args.get("link")

        if not any((aid, link)):
            return self.unwrap(
                False, data={"error": "Expected either an album ID or a link."}
            )
        elif all((aid, link)):
            return self.unwrap(
                False,
                data={
                    "error": "Got both the album ID and the link, expected only one of them."
                },
            )
        elif aid and not aid.isnumeric():
            return self.unwrap(
                False, data={"error": f"Bad ID: expected a number, got {aid}."}
            )

        try:
            self.player.play_album(aid or link)
        except UnableToPlayContentException as e:
            return self.unwrap(False, data={"error": str(e), "error_meta": e.metadata})

        return self.unwrap(True, data=self.player.page_state())

    async def play_playlist(self) -> Response:
        """
        Plays the playlist at a link.

        endpoint /play_playlist
        @param ?playlist: <str> - playlist link
        """
        playlist: Optional[str] = request.args.get("playlist")
        if not playlist:
            return self.unwrap(False, data={"error": "Expected a playlist link."})

        try:
            self.player.play_playlist(playlist)
        except UnableToPlayContentException as e:
            return self.unwrap(False, data={"error": str(e), "error_meta": e.metadata})

        return self.unwrap(True, data=self.player.page_state())

    async def play_track(self) -> Response:
        """
        Plays the track at a link.

        endpoint /play_track
        @param ?track: <str> - track link
        """
        track: Optional[str] = request.args.get("track")
        if not track:
            return self.unwrap(False, data={"error": "Expected a track link."})

        try:
            self.player.play_track(track)
        except UnableToPlayContentException as e:
            return self.unwrap(False, data={"error": str(e), "error_meta": e.metadata})

        return self.unwrap(True, data=self.player.page_state())

    async def song_history(self) -> Response:
        """
        Returns the list of all songs played during the session.

        endpoint /song_history
        """
        return self.unwrap(True, data={"history": self.player.song_history})

    async def volume(self) -> Response:
        """
        Changes the volume to the provide value and returns the result.
        If `value` is none, just returns the volume.

        endpoint /volume
        @param ?value=<float or None> - new volume
        """
        value = request.args.get("value")
        value = maybe_float(value)
        if value is not None:
            self.player.volume = value
        return self.unwrap(True, data={"volume": self.player.volume})

    async def cover(self) -> Response:
        """
        Returns the path to the track's album cover.

        :return: path to the cover
        """
        cover = self.player.save_album_cover()
        return self.unwrap(cover is not None, data={"cover": cover})

    async def song(self) -> Response:
        """
        Returns the current track.

        :return: song name
        """
        return self.unwrap(True, data={"song": self.player.song})

    async def play(self) -> Response:
        """
        Executes the play action.

        :return: action result
        """
        return self.unwrap(self.player.play())

    async def pause(self) -> Response:
        """
        Executes the pause action.

        :return: action result
        """
        return self.unwrap(self.player.pause())

    async def skip(self) -> Response:
        """
        Executes the skip action.

        :return: action result
        """
        return self.unwrap(self.player.skip())

    async def back(self) -> Response:
        """
        Executes the back action.

        :return: action result
        """
        return self.unwrap(self.player.back())

    async def like(self) -> Response:
        """
        Executes the like action.

        :return: action result.
        """
        result = self.player.like()
        return self.unwrap(result.is_ok(), data={"favorite": str(result)})

    async def hq_on(self) -> Response:
        """
        Executes the `HQ on` action.

        :return: action result.
        """
        return self.unwrap(self.player.turn_on_high_quality())

    async def hq_off(self) -> Response:
        """
        Executes the `HQ off` action.

        :return: action result.
        """
        return self.unwrap(self.player.turn_off_high_quality())

    async def shutdown(self) -> None:
        """
        Shutdowns the server.

        :raises: :exc:`Shutdown`
        """
        logger.debug("Received a shutdown request.")
        logger.debug("Pausing the track before shutting down the server.")
        await self.pause()
        raise KeyboardInterrupt
